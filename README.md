# **[Moved to Gitlab](https://gitlab-ce.alauda.cn/alauda/alauda-backend)**

## **Module update**

From `v0.2.0` ownwards this module will used an internal address and will change to `gomod.alauda.cn/alauda-backend`

For specifics:

 - [Vanity URL](http://confluence.alauda.cn/pages/viewpage.action?pageId=67540277)
 - [Go Module](http://confluence.alauda.cn/pages/viewpage.action?pageId=67541763)
 

# Alauda Backend

A simple wrapper and initializer for a go-restful webserver. Works very well with `bitbucket.org/mathildetech/app` to create full-fledged applications

## Example usage
With integrated `bitbucket.org/mathildetech/app`

```
package main

import (
	"math/rand"
	"os"
	goruntime "runtime"
	"time"

	"gomod.alauda.cn/alauda-backend/pkg/context"
	"gomod.alauda.cn/alauda-backend/pkg/decorator"
	"gomod.alauda.cn/alauda-backend/pkg/server"
	"gomod.alauda.cn/alauda-backend/pkg/server/options"
	"gomod.alauda.cn/app"
	"gomod.alauda.cn/log"
	"github.com/emicklei/go-restful"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// main func only inits app and Run
// these is a `bitbucket.org/mathildetech/app` feature
func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		goruntime.GOMAXPROCS(goruntime.NumCPU())
	}
	newApp().Run()
}

// `bitbucket.org/mathildetech/app` creator wrapper
// options are recommended options provided by the framework that
// if custimezed options are necessary, please check pkg/server/options.Optioner interface
func newApp() *app.App {
	opts := options.NewRecommendedOptions()
    // app name nad description
	application := app.NewApp("test", "test",
		app.WithOptions(opts),
		app.WithDescription("aa"),
		app.WithRunFunc(run(opts)),
	)
	return application
}

// wire up your own webservice
func run(opts options.Optioner) app.RunFunc {
	return func(name string) error {
		s := server.New(name)
		ws := testService(s)
		s.Container().Add(ws)
		if err := opts.ApplyToServer(s); err != nil {
			return err
		}
		s.Start()
		return nil
	}
}
```

# testService function

```

func testService(srv server.Server) *restful.WebService {
    // decorator package contain a few easy to use functions to 
    // create common used features and init a *restful.WebService
	ws := decorator.NewWSGenerator().New(srv)

    // this is a Filter (middleware) generator and can provide a few
    // useful models to create a kubernetes.Interface 
	clientMW := decorator.Client{srv}

    // adding the first route
	ws.Route(
		ws.GET("/v1/insecure").
            // InsecureFilter will init a InsecureClient() and add to the context
            // se pkg/client/manager.go for more details
			Filter(clientMW.InsecureFilter).
			To(func(req *restful.Request, res *restful.Response) {
                // using the above filter here we can fetch the client directly from context
				client := context.Client(req.Request.Context())
				
                // some example implementation to fetch configmaps
				list, err := client.CoreV1().ConfigMaps("alauda-system").List(metav1.ListOptions{})
				if err != nil {
					srv.L().Error("fetch cm list err", log.Err(err))
                    // server has a HandleError function that can return a standard
                    // error format based on an error
					srv.HandleError(err, req, res)
					return
				}
				
                // write configmap list as response
				res.WriteAsJson(list)
			}).Writes(corev1.ConfigMapList{}),
	)

	ws.Route(
		ws.GET("/v1/secure").
            // SecureFilter generates and injects the client based on Authorization: Bearer token
			// if --enable-multi-cluster is enabled will automatically use the --multi-cluster-param name
			// to generate a multi cluster request
			Filter(clientMW.SecureFilter).
			To(func(req *restful.Request, res *restful.Response) {
				client := context.Client(req.Request.Context())
				
                // fetchs a list of configmaps using user token
				list, err := client.CoreV1().ConfigMaps("alauda-system").List(metav1.ListOptions{})
				if err != nil {
					srv.HandleError(err, req, res)
					
				}
				
				res.WriteAsJson(list)
			}).Writes(corev1.ConfigMapList{}),
	)

	return ws
}
```

## Swagger Docs

`github.com/emicklei/go-restful` 's API can generate vary powerful and complete documentation, but it depends on its usage. See [go-restful](https://github.com/emicklei/go-restful/tree/master/examples) code for examples 


## Implementation example

- [alauda-console](https://bitbucket.org/mathildetech/alauda-console)

## Recommended options

The recommended options lives in the `pkg/server/options/recommended.go` file

By default it supports:

- Logger using zap.Logger
- pprof/debug APIs
- Prometheus metrics
- Swagger api doc and embeded swagger UI
- health check (`/healthz` and `/_ping`)
- Error handling (using kubernetes' Status object)
- kubernetes.Interface client middleware
- multi-cluster support

To see how this would translate to your app's command you can run:

```
go run main.go --help
```

```
Usage:
  test [flags]

Flags:
  -C, --config FILE                      Read configuration from specified FILE, support JSON, TOML, YAML, HCL, or Java properties formats.
      --insecure-bind-address ip         The IP address on which to serve the --insecure-port (set to 0.0.0.0 for all IPv4 interfaces and :: for all IPv6 interfaces). (default 0.0.0.0)
      --insecure-port int                The port on which to serve unsecured, unauthenticated access. (default 8080)
      --health-check                     Enables health check endpoint /healthz on server. (default true)
      --log-level LEVEL                  Minimum log output LEVEL. (default "info")
      --log-format FORMAT                Log output FORMAT, support plain or json format. (default "console")
      --log-disable-color                Disable output ansi colors in plain format logs.
      --log-enable-caller                Enable output of caller information in the log.
      --log-output-paths strings         Output paths of log (default [stdout])
      --log-error-output-paths strings   Error output paths of log (default [stderr])
      --request-log                      Enable request logs as debug (default true)
      --apiserver-host string            The address of the Kubernetes Apiserver to connect to in the format of protocol://address:port, e.g., http://localhost:8080. If not specified, the assumption is that the binary runs inside a Kubernetes cluster and local discovery is attempted.
      --kubeconfig string                Path to kubeconfig file with authorization and master location information.
      --user-agent string                User agent used by the client (default "alauda-backend")
      --enable-anonymous                 When enabled this settings will use the kubeconfig auth info or service account info instead of user login
      --qps float32                      QPS used by the client (default 1e+06)
      --burst int                        Burst used by the client (default 1000000)
      --client-timeout duration          Timeout set on client (default 30s)
      --enable-multi-cluster             Enable multi-cluster client using request's cluster parameter name or query string. If true must set multi-cluster-host as a full fledged hostname.
      --multi-cluster-host string        Multi cluster host full fledged address.
      --cluster-param-name string        Multi cluster parameter name from request. (default "cluster")
      --profiling                        Enable profiling via web interface host:port/debug/pprof/
      --contention-profiling             Enable lock contention profiling, if profiling is enabled
      --metrics                          Enable metrics for prometheus web interface host:port/metrics (default true)
      --swagger                          Enable swagger api docs on /swagger.json (default true)
      --swagger-ui                       Enable swagger ui on /swagger-ui if swagger api docs is enabled (default true)
  -V, --version version[=true]           Print version information and quit.
  -H, --help                             Help for test.
```