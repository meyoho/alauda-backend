package main

import (
	"math/rand"
	"os"
	goruntime "runtime"
	"time"

	"github.com/emicklei/go-restful"
	"gomod.alauda.cn/alauda-backend/pkg/context"
	"gomod.alauda.cn/alauda-backend/pkg/decorator"
	"gomod.alauda.cn/alauda-backend/pkg/registry"
	"gomod.alauda.cn/alauda-backend/pkg/server"
	"gomod.alauda.cn/alauda-backend/pkg/server/options"
	"gomod.alauda.cn/app"
	"gomod.alauda.cn/log"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// main func only inits app and Run
// these is a `bitbucket.org/mathildetech/app` feature
func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		goruntime.GOMAXPROCS(goruntime.NumCPU())
	}
	newApp().Run()
}

// `bitbucket.org/mathildetech/app` creator wrapper
// options are recommended options provided by the framework that
// if custimezed options are necessary, please check pkg/server/options.Optioner interface
func newApp() *app.App {
	opts := options.NewRecommendedOptions()
	// app name nad description
	application := app.NewApp("test", "test",
		app.WithOptions(opts),
		app.WithDescription("aa"),
		app.WithRunFunc(run(opts)),
	)
	return application
}

// wire up your own webservice
func run(opts options.Optioner) app.RunFunc {
	return func(name string) error {
		s := server.New(name)
		registry.AddBuilder(testService)
		if err := opts.ApplyToServer(s); err != nil {
			return err
		}
		s.Start()
		return nil
	}
}

func testService(srv server.Server) (*restful.WebService, error) {
	// decorator package contain a few easy to use functions to
	// create common used features and init a *restful.WebService
	ws := decorator.NewWSGenerator().New(srv)

	// this is a Filter (middleware) generator and can provide a few
	// useful models to create a kubernetes.Interface
	clientMW := decorator.Client{Server: srv}
	queryMW := decorator.NewQuery()
	logger := srv.L()

	// adding the first route
	ws.Route(
		// adds query parameters and filter to the restful.RouteBuilder
		queryMW.Build(
			ws.GET("/v1/insecure").
				// InsecureFilter will init a InsecureClient() and add to the context
				// se pkg/client/manager.go for more details
				Filter(clientMW.InsecureFilter).
				To(func(req *restful.Request, res *restful.Response) {
					// using the above filter here we can fetch the client directly from context
					client := context.Client(req.Request.Context())
					query := context.Query(req.Request.Context())

					// some example implementation to fetch configmaps
					list, err := client.CoreV1().ConfigMaps("alauda-system").List(metav1.ListOptions{})
					if err != nil {
						logger.Error("fetch cm list err", log.Err(err))
						// server has a HandleError function that can return a standard
						// error format based on an error
						srv.HandleError(err, req, res)
						return
					}
					// filter items using defined query
					// QueryItems will use a standard ObjectDataCell
					// and only use predefined attributes declared in it
					filtered, count := queryMW.QueryItems(list.Items, query)
					// TODO: need to find a better way to convert items
					// without having to declare a function for that
					list.Items = convertToConfigMapSlice(filtered)
					queryMW.AddItemCountHeader(res, count)

					// write configmap list as response
					res.WriteAsJson(list)
				}).Writes(corev1.ConfigMapList{}),
		),
	)

	ws.Route(
		ws.GET("/v1/secure").
			// SecureFilter generates and injects the client based on Authorization: Bearer token
			// if --enable-multi-cluster is enabled will automatically use the --multi-cluster-param name
			// to generate a multi cluster request
			Filter(clientMW.SecureFilter).
			To(func(req *restful.Request, res *restful.Response) {
				client := context.Client(req.Request.Context())

				// fetchs a list of configmaps using user token
				list, err := client.CoreV1().ConfigMaps("alauda-system").List(metav1.ListOptions{})
				if err != nil {
					srv.HandleError(err, req, res)
					return
				}
				res.WriteAsJson(list)
			}).Writes(corev1.ConfigMapList{}),
	)

	ws.Route(
		ws.GET("/v1/dynamic").
			// DynamicFilterGenerator generates and injects the DynamicClient based on Authorization: Bearer token
			// if --enable-multi-cluster is enabled will automatically use the --multi-cluster-param name
			// to generate a multi cluster request
			Filter(clientMW.DynamicFilterGenerator(&schema.GroupVersionKind{Group: "", Kind: "Secret", Version: "v1"})).
			To(func(req *restful.Request, res *restful.Response) {
				client := context.DynamicClient(req.Request.Context())
				list, err := client.Namespace("alauda-system").List(metav1.ListOptions{})
				if err != nil {
					srv.HandleError(err, req, res)
					return
				}
				res.WriteAsJson(list)
			}).Writes(corev1.ConfigMapList{}),
	)

	return ws, nil
}

// convertToConfigMapSlice TODO: need to find a better way to convert items
// without having to do it manually
func convertToConfigMapSlice(filtered []metav1.Object) (items []corev1.ConfigMap) {
	items = make([]corev1.ConfigMap, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*corev1.ConfigMap); ok {
			items = append(items, *cm)
		}
	}
	return
}
