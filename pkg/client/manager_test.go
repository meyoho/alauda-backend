package client_test

import (
	"fmt"

	restful "github.com/emicklei/go-restful"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"gomod.alauda.cn/alauda-backend/pkg/client"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

var _ = Describe("Manager.InsecureClient", func() {
	var (
		mgr = client.NewManager()
		// req *restful.Request
		k8sClient kubernetes.Interface
		err       error
		logger    *zap.Logger
	)

	BeforeEach(func() {
		logger, _ = zap.NewDevelopment()
		mgr.WithConfig(&client.Config{
			KubeAPIServer:  "https://localhost:6443",
			KubeConfigPath: "$HOME/.kube/config",
			Log:            logger,
		})
	})

	JustBeforeEach(func() {
		k8sClient, err = mgr.InsecureClient()
	})

	Describe("insecure generator", func() {
		Context("simple config generator", func() {
			BeforeEach(func() {
				mgr.InsecureConfigGeneratorFuncs = []client.ConfigGenFunc{}
				mgr.WithInsecure(func(cfg *client.Config, req *restful.Request) (*rest.Config, error) {
					// some basic empty stuff
					return &rest.Config{}, nil
				})
			})

			It("should provide", func() {
				Expect(err).To(BeNil(), "shouldnot error")
				Expect(k8sClient).ToNot(BeNil(), "should generate client")
			})
		})
		Context("config func returns error", func() {
			BeforeEach(func() {
				mgr.InsecureConfigGeneratorFuncs = []client.ConfigGenFunc{}
				mgr.WithInsecure(func(cfg *client.Config, req *restful.Request) (*rest.Config, error) {
					return nil, fmt.Errorf("some error here!")
				})
			})

			It("should return error", func() {
				Expect(err).ToNot(BeNil(), "should return error")
				Expect(k8sClient).To(BeNil(), "should NOT generate client")
			})
		})

	})

})
