package server

import (
	"net"

	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"gomod.alauda.cn/alauda-backend/pkg/client"
)

// Server interface for the server of this framework
type Server interface {
	Container() *restful.Container
	Start()

	Logger
	Manager
	ErrorHandler
}

// ListenerSetter sets lister for server
type ListenerSetter interface {
	SetListener(net.Listener, int)
}

// PathPrefixSetter sets a path prefix
type PathPrefixSetter interface {
	SetPathPrefix(string)
}

// Logger interface to manage logger
type Logger interface {
	LoggerSetter
	LoggerGetter
}

// LoggerSetter sets a zap.Logger
type LoggerSetter interface {
	SetLogger(*zap.Logger)
}

// LoggerGetter gets a zap.Logger instance
type LoggerGetter interface {
	L() *zap.Logger
	Log() *zap.Logger
}

// Manager interface for setting a client manager into the server
type Manager interface {
	ManagerSetter
	ManagerGetter
}

// ManagerSetter sets a client.Manager
type ManagerSetter interface {
	SetManager(client.Manager)
}

// ManagerGetter interface to get a client.Manager
type ManagerGetter interface {
	GetManager() client.Manager
}

// ErrorHandler handle errors for requests
type ErrorHandler interface {
	HandleError(err error, req *restful.Request, res *restful.Response)
	ErrorHandlerSetter
}

// ErrorHandlerFunc error handling function
type ErrorHandlerFunc func(err error, req *restful.Request, res *restful.Response)

// ErrorHandlerSetter interface to set an error handler
type ErrorHandlerSetter interface {
	SetErrorHandler(ErrorHandlerFunc)
}
