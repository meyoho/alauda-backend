package options

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/api/errors"
)

var _ = Describe("handleError", func() {
	var (
		err      error
		returned *errors.StatusError
	)

	JustBeforeEach(func() {
		returned = handleError(err)
	})

	Describe("err is nil", func() {
		BeforeEach(func() {
			err = nil
		})

		It("should return InternalServerError", func() {
			Expect(returned).ToNot(BeNil())
			Expect(errors.IsInternalError(returned)).To(BeTrue())
		})
	})

	Describe("err not StatusError", func() {
		BeforeEach(func() {
			err = fmt.Errorf("some random error")
		})

		It("should return InternalServerError", func() {
			Expect(returned).ToNot(BeNil())
			Expect(errors.IsInternalError(returned)).To(BeTrue())
			Expect(returned.Error()).To(ContainSubstring("some random error"))
		})
	})

	Describe("err is errors.StatusError", func() {
		BeforeEach(func() {
			err = errors.NewBadRequest("some bad request")
		})

		It("should return InternalServerError", func() {
			Expect(returned).ToNot(BeNil())
			Expect(errors.IsBadRequest(returned)).To(BeTrue())
			Expect(returned.Error()).To(Equal("some bad request"))
		})
	})
})
