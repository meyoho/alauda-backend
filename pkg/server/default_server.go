package server

import (
	"fmt"
	"net"
	"net/http"
	"sync"

	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"gomod.alauda.cn/alauda-backend/pkg/client"
)

// DefaultServer default implementation for server
type DefaultServer struct {
	container *restful.Container
	logger    *zap.Logger
	logLock   *sync.RWMutex
	listener  net.Listener
	manager   client.Manager
	mgrLock   *sync.RWMutex
	bindPort  int

	errHandler ErrorHandlerFunc
}

var _ Server = &DefaultServer{}

// New initializes a new  default server
func New(name string) Server {
	logger, _ := zap.NewProduction()
	if logger == nil {
		logger = zap.NewNop()
	}
	server := &DefaultServer{
		container: restful.NewContainer(),
		logger:    logger,
		logLock:   &sync.RWMutex{},
		mgrLock:   &sync.RWMutex{},
	}
	return server
}

// Container returns container of server
func (s *DefaultServer) Container() *restful.Container {
	return s.container
}

// Start starts server
func (s *DefaultServer) Start() {
	if s.bindPort <= 0 {
		s.bindPort = 8080
	}
	if s.listener != nil {
		// TODO: give options to enable a more complex server
		// instead of using the default one
		http.Serve(s.listener, s.Container())
	} else {
		http.ListenAndServe(fmt.Sprintf(":%d", s.bindPort), s.Container())
	}
}

var _ ListenerSetter = &DefaultServer{}

// SetListener sets a listener
func (s *DefaultServer) SetListener(listener net.Listener, bindPort int) {
	s.listener = listener
	s.bindPort = bindPort
}

var _ LoggerSetter = &DefaultServer{}

// SetLogger sets a zap.Logger instance on server
func (s *DefaultServer) SetLogger(logg *zap.Logger) {
	s.logLock.Lock()
	defer s.logLock.Unlock()
	s.logger = logg
}

// Log returns a zap.Logger
func (s *DefaultServer) Log() *zap.Logger {
	return s.L()
}

// L returns a zap.Logger
func (s *DefaultServer) L() *zap.Logger {
	s.logLock.RLock()
	defer s.logLock.RUnlock()
	return s.logger
}

// SetManager sets a client manager on the server
func (s *DefaultServer) SetManager(mgr client.Manager) {
	s.mgrLock.Lock()
	defer s.mgrLock.Unlock()
	s.manager = mgr
}

// GetManager gets a client manager from the server
func (s *DefaultServer) GetManager() client.Manager {
	s.mgrLock.RLock()
	defer s.mgrLock.RUnlock()
	return s.manager
}

// HandleError handle request errors
func (s *DefaultServer) HandleError(err error, req *restful.Request, res *restful.Response) {
	s.errHandler(err, req, res)
}

// SetErrorHandler sets error handler for request errors
func (s *DefaultServer) SetErrorHandler(errHandler ErrorHandlerFunc) {
	s.errHandler = errHandler
}
