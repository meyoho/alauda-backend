package dataselect_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDataselect(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "gomod.alauda.cn/alauda-backend/pkg/dataselect")
}
