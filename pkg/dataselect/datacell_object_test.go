package dataselect_test

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	api "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	ds "gomod.alauda.cn/alauda-backend/pkg/dataselect"
)

var _ = Describe("DatacellObject", func() {
	var (
		data     ds.ObjectDataCell
		name     ds.PropertyName
		value    ds.ComparableValue
		creation = time.Now()
	)

	BeforeEach(func() {
		data = ds.ObjectDataCell{
			Object: &api.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "configmap",
					Namespace:         "system",
					CreationTimestamp: metav1.Time{Time: creation},
					Labels: map[string]string{
						"a": "b",
						"c": "d",
					},
					Annotations: map[string]string{
						"annotation": "content",
					},
				},
			},
		}
	})

	JustBeforeEach(func() {
		value = data.GetProperty(name)
	})
	Context("Nil", func() {
		BeforeEach(func() { data.Object = nil })
		It("should be nil", func() {
			Expect(data.Object).To(BeNil())
			Expect(value).To(BeNil())
		})
	})
	Context("Name", func() {
		BeforeEach(func() { name = "name" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableContainsString("configmap")))
		})
	})
	Context("Namespace", func() {
		BeforeEach(func() { name = "namespace" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableString("system")))
		})
	})
	Context("CreationTimestamp", func() {
		BeforeEach(func() { name = "creationTimestamp" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableTime(creation)))
		})
	})
	Context("Label", func() {
		BeforeEach(func() { name = "labels" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			// not possible to make reading of a map consistent
			// reading of a map could have different order so adding both possibilities here
			Expect(value).To(Or(Equal(ds.StdComparableLabel("a:b,c:d")), Equal(ds.StdComparableLabel("c:d,a:b"))))
		})
	})
	Context("Annotation", func() {
		BeforeEach(func() { name = "annotations" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableLabel("annotation:content")))
		})
	})
})

var _ = Describe("DatacellObjectWithGetter", func() {
	var (
		data     ds.ObjectDataCell
		name     ds.PropertyName
		value    ds.ComparableValue
		creation = time.Now()
	)

	BeforeEach(func() {
		data = ds.ObjectDataCell{
			Object: &api.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "configmap",
					Namespace:         "system",
					CreationTimestamp: metav1.Time{Time: creation},
					Labels: map[string]string{
						"a": "b",
						"c": "d",
					},
					Annotations: map[string]string{
						"annotation":  "content",
						"displayName": "Extendable",
					},
				},
			},
			Getters: []ds.PropertyGetter{
				{
					Name: "displayName",
					F: func(obj interface{}) ds.ComparableValue {
						cfg := obj.(*api.ConfigMap)
						return ds.StdComparableContainsString(cfg.Annotations["displayName"])
					},
				},
			},
		}
	})

	JustBeforeEach(func() {
		value = data.GetProperty(name)
	})
	Context("Name", func() {
		BeforeEach(func() { name = "name" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableContainsString("configmap")))
		})
	})
	Context("Namespace", func() {
		BeforeEach(func() { name = "namespace" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableString("system")))
		})
	})
	Context("displayName", func() {
		BeforeEach(func() { name = "displayName" })
		It("should not be blank", func() {
			Expect(data).ToNot(BeNil())
			Expect(value).To(Equal(ds.StdComparableContainsString("Extendable")))
		})
	})
})

var _ = Describe("ToObjectCellSlice/FromCellToObjectSlice", func() {
	var (
		slice         interface{}
		expected      []ds.DataCell
		list          *api.ConfigMapList
		convertedBack []metav1.Object
	)
	JustBeforeEach(func() {
		expected = ds.ToObjectCellSlice(slice)
		convertedBack = ds.FromCellToObjectSlice(expected)
	})
	Context("ConfigMapList", func() {
		BeforeEach(func() {
			list = &api.ConfigMapList{
				Items: []api.ConfigMap{
					{
						ObjectMeta: metav1.ObjectMeta{
							Name:        "configmap",
							Namespace:   "system",
							Labels:      map[string]string{"a": "b", "c": "d"},
							Annotations: map[string]string{"annotation": "content"},
						},
					},
				},
			}
			slice = list.Items
		})
		It("should return a slice with one item", func() {
			Expect(expected).ToNot(BeNil())
			Expect(expected).To(HaveLen(1))
			Expect(expected[0]).To(Equal(ds.ObjectDataCell{Object: &list.Items[0]}))

			Expect(convertedBack).ToNot(BeNil())
			Expect(convertedBack).To(HaveLen(1))
		})
	})

	Context("ConfigMapList items is nil", func() {
		BeforeEach(func() {
			list = &api.ConfigMapList{
				Items: nil,
			}
			slice = list.Items
		})
		It("should return an empty slice", func() {
			Expect(expected).ToNot(BeNil())
			Expect(expected).To(HaveLen(0))
		})
	})

	Context("Other object kind", func() {
		BeforeEach(func() {
			type testStruct struct{}
			slice = testStruct{}
		})
		It("should return an empty slice", func() {
			Expect(expected).ToNot(BeNil())
			Expect(expected).To(HaveLen(0))
		})
	})
})

var _ = Describe("ToObjectCellSlice/FromCellToUnstructuredSlice", func() {
	var (
		slice         interface{}
		expected      []ds.DataCell
		list          *api.ConfigMapList
		convertedBack []unstructured.Unstructured
	)
	JustBeforeEach(func() {
		expected = ds.ToObjectCellSlice(slice)
		convertedBack = ds.FromCellToUnstructuredSlice(expected)
	})
	Context("ConfigMapList", func() {
		BeforeEach(func() {
			list = &api.ConfigMapList{
				Items: []api.ConfigMap{
					{
						TypeMeta: metav1.TypeMeta{
							Kind:       "ConfigMap",
							APIVersion: "apps/v1",
						},
						ObjectMeta: metav1.ObjectMeta{
							Name:        "configmap",
							Namespace:   "system",
							Labels:      map[string]string{"a": "b", "c": "d"},
							Annotations: map[string]string{"annotation": "content"},
						},
					},
				},
			}
			slice = list.Items
		})
		It("should return a slice with one item", func() {
			Expect(expected).ToNot(BeNil())
			Expect(expected).To(HaveLen(1))
			Expect(expected[0]).To(Equal(ds.ObjectDataCell{Object: &list.Items[0]}))

			Expect(convertedBack).ToNot(BeNil())
			Expect(convertedBack).To(HaveLen(1))
		})
	})

})
