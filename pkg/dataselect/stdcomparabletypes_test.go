package dataselect

import (
	"reflect"

	"time"

	. "github.com/onsi/ginkgo"
)

var _ = Describe("StdComparableTypes", func() {
	It("ints compare", func() {
		cases := []struct {
			a, b, expected int
		}{
			{
				5, 1, 1,
			},
			{
				5, 5, 0,
			},
			{
				1, 3, -1,
			},
		}
		for _, c := range cases {
			actual := intsCompare(c.a, c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("intsCompare(%+v, %+v) == %+v, expected %+v", c.a, c.b, actual, c.expected)
			}
		}
	})
	It("ints64 compare", func() {
		cases := []struct {
			a, b     int64
			expected int
		}{
			{
				5, 1, 1,
			},
			{
				5, 5, 0,
			},
			{
				1, 3, -1,
			},
		}
		for _, c := range cases {
			actual := ints64Compare(c.a, c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("ints64Compare(%+v, %+v) == %+v, expected %+v", c.a, c.b, actual, c.expected)
			}
		}
	})
	It("std comparable time contains", func() {
		now := time.Now()
		future := now.Add(894718949849)
		cases := []struct {
			a, b     ComparableValue
			expected bool
		}{
			{
				StdComparableTime(now),
				StdComparableTime(now),
				true,
			},
			{
				StdComparableTime(now),
				StdComparableTime(future),
				false,
			},
		}
		for _, c := range cases {
			actual := c.a.Contains(c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("Contains(%+v) == %+v, expected %+v", c.b, actual, c.expected)
			}
		}
	})
	It("std comparable int contains", func() {
		cases := []struct {
			a, b     StdComparableInt
			expected bool
		}{
			{
				StdComparableInt(3),
				StdComparableInt(3),
				true,
			},
			{
				StdComparableInt(1),
				StdComparableInt(3),
				false,
			},
		}
		for _, c := range cases {
			actual := c.a.Contains(c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("Contains(%+v) == %+v, expected %+v", c.b, actual, c.expected)
			}
		}
	})
	It("std comparable string contains", func() {
		cases := []struct {
			a, b     StdComparableString
			expected bool
		}{
			{
				StdComparableString("abc"),
				StdComparableString("abc"),
				true,
			},
			{
				StdComparableString("abc"),
				StdComparableString("xyz"),
				false,
			},
		}
		for _, c := range cases {
			actual := c.a.Contains(c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("Contains(%+v) == %+v, expected %+v", c.b, actual, c.expected)
			}
		}
	})
	It("std comparable string in contains", func() {
		cases := []struct {
			a        StdComparableStringIn
			b        StdComparableString
			expected bool
		}{
			{
				StdComparableStringIn("aa"),
				StdComparableString("aa:bb"),
				true,
			},
			{
				StdComparableStringIn("cc"),
				StdComparableString("aa:bb"),
				false,
			},
		}
		for _, c := range cases {
			actual := c.a.Contains(c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("Contains(%+v) == %+v, expected %+v", c.b, actual, c.expected)
			}
		}
	})
	It("std comparable r f c3339 timestamp", func() {
		cases := []struct {
			a, b     StdComparableRFC3339Timestamp
			expected bool
		}{
			{
				StdComparableRFC3339Timestamp("2011-08-30T13:22:53.108Z"),
				StdComparableRFC3339Timestamp("2011-08-30T13:22:53.108Z"),
				true,
			},
			{
				StdComparableRFC3339Timestamp("2011-08-30T13:22:53.108Z"),
				StdComparableRFC3339Timestamp("2018-08-30T13:22:53.108Z"),
				false,
			},
		}
		for _, c := range cases {
			actual := c.a.Contains(c.b)
			if !reflect.DeepEqual(actual, c.expected) {
				GinkgoT().Errorf("Contains(%+v) == %+v, expected %+v", c.b, actual, c.expected)
			}
		}
	})
	It("labels compare", func() {
		cases := []struct {
			data     StdComparableLabel
			query    StdComparableString
			expected bool
		}{
			{
				"lang:java,category:Build,source:customer", "lang:Java", true,
			},
			{
				"lang:java,category:Build,source:customer", "lang:java", true,
			},
			{
				"category:Build,source:customer", "lang:java", false,
			},
			{
				"category:Build,source:customer", "", false,
			},
			{
				"", "lang:java", false,
			},
		}
		for _, item := range cases {
			actual := item.data.Contains(item.query)
			if !reflect.DeepEqual(actual, item.expected) {
				GinkgoT().Errorf("intsCompare(%+v, %+v) == %+v, expected %+v", item.data, item.query, actual, item.expected)
			}
		}
	})
	It("mutil comparablevalue contains", func() {
		cases := []struct {
			data     MutilComparableValue
			query    StdComparableString
			expected bool
		}{
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				query:    StdComparableString("name"),
				expected: true,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				query:    StdComparableString("secret"),
				expected: true,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("pipelineconfig-namespace"), StdCaseInSensitiveComparableString("pipelineconfig-cluster")}},
				query:    StdComparableString("config"),
				expected: true,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("pipeline-namespace"), StdCaseInSensitiveComparableString("pipelineconfig-cluster")}},
				query:    StdComparableString("config"),
				expected: true,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("pipeline-namespace"), StdCaseInSensitiveComparableString("pipelineconfig-cluster")}},
				query:    StdComparableString("notmatch"),
				expected: false,
			},
		}

		for _, item := range cases {
			actual := item.data.Contains(item.query)
			if !reflect.DeepEqual(actual, item.expected) {
				GinkgoT().Errorf("mutil comparablevalue  Contains(%+v, %+v) == %+v, expected %+v", item.data, item.query, actual, item.expected)
			}
		}

	})

	It("mutil comparablevalue compare", func() {
		cases := []struct {
			data     MutilComparableValue
			query    MutilComparableValue
			expected int
		}{
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				query:    MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				expected: 0,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("anamespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				query:    MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("bnamespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				expected: -1,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("bnamespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				query:    MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("anamespace-secret"), StdCaseInSensitiveComparableString("cluster-secret")}},
				expected: 1,
			},
			{
				data:     MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("acluster-secret")}},
				query:    MutilComparableValue{[]ComparableValue{StdCaseInSensitiveComparableString("namespace-secret"), StdCaseInSensitiveComparableString("bcluster-secret")}},
				expected: -1,
			},
		}

		for _, item := range cases {
			actual := item.data.Compare(item.query)
			if !reflect.DeepEqual(actual, item.expected) {
				GinkgoT().Errorf("intsCompare(%+v, %+v) == %+v, expected %+v", item.data, item.query, actual, item.expected)
			}
		}

	})

})
