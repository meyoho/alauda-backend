# Copyright 2019 THL A29 Limited, a Tencent company.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PHONY: all
all: lint test

# ==============================================================================
# Build Options

# set the shell to bash in case some environments use sh
SHELL := /bin/bash
GO ?= $(shell which go)
PACKAGES ?= $(shell $(GO) list ./...)
GOFILES := $(shell find . -name "*.go" -type f)
GOFMT ?= gofmt "-s"
export GOPROXY=https://athens.alauda.cn
export GO111MODULE=on
export GONOSUMDB="gitlab-ce.alauda.cn/*,gomod.alauda.cn/*"
GOMOD ?= $(GOPROXY=https://athens.alauda.cn GO111MODULE=on $(GO))

# ==============================================================================
# Tasks

.PHONY: fmt
fmt:
	@$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check:
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

.PHONY: vet
vet:
	$(GO) vet $(PACKAGES)

.PHONY: lint
lint:
	@hash revive > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		$(GO) get -u github.com/mgechev/revive; \
	fi
	@revive -config ./build/linter/revive.toml -exclude ./pkg/server/options/assets/... ./... || exit 1

.PHONY: test
test: fmt-check
	@mkdir -p ./output
	 $(GO) test -v -cover -coverprofile ./output/coverage.out $(PACKAGES) || exit 1

.PHONY: clean
clean:
	$(GO) clean -x -i ./...
	@rm -rf ./output/

.PHONY: check-gobindata
check-gobindata:
	@hash go-bindata > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		$(GO) get -u github.com/jteeuwen/go-bindata/...; \
	fi

.PHONY: swagger-ui
swagger-ui: check-gobindata
	go-bindata -nocompress -pkg swagger -o ./pkg/server/options/assets/swagger/datafile.go -prefix /third_party/swagger-ui/ third_party/swagger-ui/...
	gofmt -s -w ./pkg/server/options/assets/swagger/datafile.go